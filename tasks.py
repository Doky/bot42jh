""" Invoke Tasks """

import os

from invoke import task

ROOT_DIR = os.path.dirname(__file__)


@task
def patch_pythonpath(ctx):
    """
    Patches PYTHONPATH.
    Adds the application location to the path.
    """
    if os.environ.get("PYTHONPATH") is None:
        os.environ["PYTHONPATH"] = os.path.join(ROOT_DIR)
    else:
        os.environ["PYTHONPATH"] += os.pathsep + os.path.join(ROOT_DIR)


@task(patch_pythonpath)
def isort(ctx):
    """
    Runs isort
    Format the imports.
    """
    ctx.run("isort *.py")


@task(patch_pythonpath)
def black(ctx):
    """
    Runs Black
    Format the code.
    """
    ctx.run("black *.py")
