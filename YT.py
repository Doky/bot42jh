import argparse

import httplib2

# Google Data API
from googleapiclient.discovery import build

import oauth2client
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run_flow


class YoutubeAdapter(object):
    """An adapter class for the Youtube service. This class presents the API
    that our script logic needs and handles the interaction with the Youtube
    servers."""

    YOUTUBE_READ_WRITE_SCOPE = "https://www.googleapis.com/auth/youtube"
    YOUTUBE_API_SERVICE_NAME = "youtube"
    YOUTUBE_API_VERSION = "v3"
    REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob"

    def __init__(self, logger, api_key, config_path):
        """Create an object which contains an instance of the YouTube service
        from the Google Data API library"""
        self.logger = logger

        client_secrets_file = config_path + "client_secrets.json"
        missing_secrets_message = "Error: {0} is missing".format(client_secrets_file)

        # Do OAuth2 authentication
        flow = flow_from_clientsecrets(
            client_secrets_file,
            message=missing_secrets_message,
            scope=YoutubeAdapter.YOUTUBE_READ_WRITE_SCOPE,
            redirect_uri=YoutubeAdapter.REDIRECT_URI,
        )

        storage = Storage(config_path + "oauth2.json")
        credentials = storage.get()

        if credentials is None or credentials.invalid:
            parser = argparse.ArgumentParser(
                description=__doc__,
                formatter_class=argparse.RawDescriptionHelpFormatter,
                parents=[oauth2client.tools.argparser],
            )
            flags = parser.parse_args()

            credentials = run_flow(flow, storage, flags)

        # Create the service to use throughout the script
        self.service = build(
            YoutubeAdapter.YOUTUBE_API_SERVICE_NAME,
            YoutubeAdapter.YOUTUBE_API_VERSION,
            developerKey=api_key,
            http=credentials.authorize(httplib2.Http()),
        )

    def get_video_id_for_search(self, query):
        """Returns the videoId of the first search result if at least one video
        was found by searching for the given query, otherwise returns
        None"""

        search_response = (
            self.service.search()
            .list(
                q=query,
                part="id",
                maxResults=3,
                safeSearch="none",
                type="video",
                fields="items",
            )
            .execute()
        )

        items = search_response.get("items", [])
        if not items:
            return None

        for item in items:
            # The "type" parameter doesn't always work for some reason, so we
            # have to check each item for its type.
            if item["id"]["kind"] == "youtube#video":
                return item["id"]["videoId"]
            else:
                self.logger.warning(
                    "\tResult is not a video, continuing to next result"
                )

        return None

    def add_video_to_playlist(self, pl_id, video_id):
        """Adds the given video as the last video as the last one in the given
        playlist"""
        self.logger.info("\tAdding video pl_id: %s video_id: %s", pl_id, video_id)

        video_insert_response = (
            self.service.playlistItems()
            .insert(
                part="snippet",
                body=dict(
                    snippet=dict(
                        playlistId=pl_id,
                        resourceId=dict(kind="youtube#video", videoId=video_id),
                    )
                ),
                fields="snippet",
            )
            .execute()
        )

        title = video_insert_response["snippet"]["title"]

        self.logger.info("\tVideo added: %s", title.encode("utf-8"))

    def create_new_playlist(self, title, description):
        """Creates a new, empty YouTube playlist with the given title and
        description"""
        playlists_insert_response = (
            self.service.playlists()
            .insert(
                part="snippet,status",
                body=dict(
                    snippet=dict(title=title, description=description),
                    status=dict(privacyStatus="public"),
                ),
                fields="id",
            )
            .execute()
        )

        pl_id = playlists_insert_response["id"]
        pl_url = self._playlist_url_from_id(pl_id)

        self.logger.info("New playlist added: %s", title)
        self.logger.info("\tID: %s", pl_id)
        self.logger.info("\tURL: %s", pl_url)

        return pl_id

    def playlist_exists_with_title(self, title):
        """Returns true if there is already a playlist in the channel with the
        given name"""
        playlists = (
            self.service.playlists()
            .list(part="snippet", mine=True, maxResults=10, fields="items")
            .execute()
        )

        for playlist in playlists["items"]:
            if playlist["snippet"]["title"] == title:
                return True
        return False

    @staticmethod
    def _playlist_url_from_id(pl_id):
        """Returns the URL of a playlist, given its ID"""
        return "https://www.youtube.com/playlist?list={0}".format(pl_id)
