import os

import discord
from main import parse_playlists_and_upload
from parse_file import parse_and_create_playlists_files

guild = discord.Guild

client = discord.Client()

# Plateforms to implement (YT, Spotify, Soundcloud, Bandcamp)


async def my_background_task():
    await client.wait_until_ready()
    channel_names = ["🎹instrumental", "🎙vocal", "🎼other", "🎮video-game"]
    for channel_name in channel_names:
        channel = discord.utils.get(client.get_all_channels(), name=channel_name)
        messages = await channel.history(limit=1000000).flatten()

        lines = []
        out_file_name = channel_name + ".txt"
        out_file_path = os.path.join("raw", out_file_name)
        if os.path.exists(out_file_path):
            with open(out_file_path, "r") as fd:
                lines = fd.readlines()
        with open(out_file_path, "a") as fd:
            for message in messages:
                content = message.content.strip().replace(os.linesep, " ") + os.linesep
                if "https" in content and content not in lines:
                    fd.write(content)
        parse_and_create_playlists_files(channel_name)
        parse_playlists_and_upload(channel_name)
    await client.close()


@client.event
async def on_ready():
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("------")


client.loop.create_task(my_background_task())
TOKEN_BOT = "token-bot"
client.run(TOKEN_BOT)
