import json
import logging
import os
import re

import discord
from YT import YoutubeAdapter

logging.basicConfig(level=logging.INFO)

client = discord.Client()


TOKEN_BOT = "token-bot"
API_KEY = "api-key"
OUT_FOLDER_PATH = "playlists"


client = discord.Client()
yt_adapter = YoutubeAdapter(logging.getLogger(), API_KEY, "./")

PLAYLIST_NUMBER = 0
PLAYLIST_TITLE = "HouseOfBob Instrumentals " + str(PLAYLIST_NUMBER)
YT_REGEX = re.compile(
    "^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*"
)


def parse_playlists_and_upload(channel_name):
    in_file_name = channel_name + ".json"
    in_file_path = os.path.join("playlists", in_file_name)

    relation_title_list_dict = {}
    relations_pl_ids_dict = {}
    relations_pl_id_videos_ids_dict = {}

    FILE_PATH = in_file_path
    RELATIONS_FILE_PATH = os.path.join(
        "playlists", channel_name + "_relations_pl_ids.json"
    )
    RELATIONS_IDS_FILE_PATH = os.path.join(
        "playlists", channel_name + "_relations_pl_id_videos_ids.json"
    )

    if os.path.exists(FILE_PATH):
        with open(FILE_PATH, "r") as relations_file:
            relation_title_list_dict = json.load(relations_file)

    if os.path.exists(RELATIONS_FILE_PATH):
        with open(RELATIONS_FILE_PATH, "r") as relations_pl_ids_file:
            relations_pl_ids_dict = json.load(relations_pl_ids_file)

    if os.path.exists(RELATIONS_IDS_FILE_PATH):
        with open(RELATIONS_IDS_FILE_PATH, "r") as relations_pl_id_videos_ids:
            relations_pl_id_videos_ids_dict = json.load(relations_pl_id_videos_ids)

    for playlist_title in relation_title_list_dict.keys():
        if playlist_title in relations_pl_ids_dict.keys():
            for video_id in relation_title_list_dict[playlist_title]:
                pl_id = relations_pl_ids_dict[playlist_title]
                if not pl_id in relations_pl_id_videos_ids_dict.keys():
                    relations_pl_id_videos_ids_dict[pl_id] = []
                if video_id not in relations_pl_id_videos_ids_dict[pl_id]:
                    try:
                        yt_adapter.add_video_to_playlist(pl_id, video_id)
                        relations_pl_id_videos_ids_dict[pl_id].append(video_id)
                    except Exception as e:
                        print(str(e), str(video_id))
        else:
            try:
                if not yt_adapter.playlist_exists_with_title(playlist_title):
                    playlist_id = yt_adapter.create_new_playlist(
                        playlist_title, playlist_title
                    )
                    relations_pl_ids_dict[playlist_title] = playlist_id
            except Exception as e:
                print(str(e), str(playlist_title))

    with open(RELATIONS_FILE_PATH, "w") as relations_pl_ids_file:
        relations_pl_ids_file.write(json.dumps(relations_pl_ids_dict, indent=4))
    with open(RELATIONS_IDS_FILE_PATH, "w") as relations_pl_id_videos_ids_file:
        relations_pl_id_videos_ids_file.write(
            json.dumps(relations_pl_id_videos_ids_dict, indent=4)
        )
