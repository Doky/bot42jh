import itertools
import json
import os
import re

import pandas

YT_REGEX = re.compile(
    "^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$"
)
OUT_FOLDER_PATH = "playlists"
MAX_PLAYLISTS_SIZE = 4999


def get_youtube_links_from_line(line):
    """
    Get every YT links in a line
    """
    links = []
    splitted = str(line).strip().split(" ")
    for split in splitted:
        link = YT_REGEX.search(split)
        if link:
            links.append(link)
    return links


def get_youtube_ids(links):
    """
    Links = list 2d
    Returns a list IDs for each links list
    """
    ids = []
    links = sum(links, [])
    not_an_id = ["c", "user", "watch"]
    for link in links:
        id = link.group(5)
        if id not in not_an_id:
            ids.append(id)
    return ids


def parse_and_create_playlists_files(channel_name):
    lines = []
    playlist_number = -1
    in_file_name = channel_name + ".txt"
    in_file_path = os.path.join("raw", in_file_name)
    if os.path.exists(in_file_path):
        with open(in_file_path, "r") as fd:
            lines = fd.readlines()
    links = []
    # For each line
    for line in lines:
        # append a list of YT links
        links.append(get_youtube_links_from_line(line))
    ids = list(set(get_youtube_ids(links)))
    relation_title_list_dict = {}
    for i, id in enumerate(ids):
        if i == 0 or i == MAX_PLAYLISTS_SIZE:
            playlist_number += 1
            playlist_title = channel_name + " " + str(playlist_number)
            relation_title_list_dict[playlist_title] = []
        relation_title_list_dict[playlist_title].append(id)

    out_file_path = os.path.join(OUT_FOLDER_PATH, channel_name + ".json")
    with open(out_file_path, "w") as outfile:
        outfile.write(json.dumps(relation_title_list_dict, indent=4))
